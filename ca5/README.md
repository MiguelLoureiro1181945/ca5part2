# Class Assignment 5 Report

# Part 1 :

1.Analysis
------------

Using **Jenkins** create a simple pipeline

2.Setup
----------

Here I describe the initial setup before going to implementation.

1. Download _jenkins.war_ file from [here](https://www.jenkins.io/download/). I select the file for _Windows_.

2. Copy the jenkins.war_ file to the class assignment folder (i.e. .../ca5)

3.Install **Jenkins** with the command `java -jar jenkins.war`

![jenkinsinstall](https://i.imgur.com/vKyOXsu.png)

![jenkinsinstall2](https://i.imgur.com/4UxWKQM.png)

*Note: open http://localhost:8080 to access Jenkins and execute any necessary additional setup step

3.Jobs description
------------

Create a very simple pipeline using the "gradle_basic_demo" application



4.Implementation
----------
Each task steps is described below.


##### Step 1: Create a new _pipeline_ job 



1. jgf

##### Task 2:

1.



##### Task 3:


5.Conclusion
----------


6.Finish the class assignment
-------

* Add tag `ca5-part1` to the repository

---------------------------------------------------------------------------------

# Class Assignment 5 Report

# Part 2 :

1.Analysis
------------


2.Implementation
----------
Using 

1. Copy  _.../ca3/part2_.

3.Alternative technological solution
----------

1. xsdgsdg


4.Conclusion
-------

### Considerations 



### Differences



5.Finish the class assignment
----------

* Add tag `ca5-part2` to the repository