# Class Assignment 5 Report

# Part 2 :

1.Analysis
------------

Using **Jenkins** create a simple pipeline

2.Setup
----------

Here I describe the initial setup before going to implementation.

1. Download _jenkins.war_ file from [here](https://www.jenkins.io/download/). I select the file for _Windows_.

2. Copy the jenkins.war_ file to the class assignment folder (i.e. .../ca5)

3.Install **Jenkins** with the command `java -jar jenkins.war`

![jenkinsinstall](https://i.imgur.com/vKyOXsu.png)

![jenkinsinstall2](https://i.imgur.com/4UxWKQM.png)

*Note: _Jenkins_ is installed. Please open http://localhost:8080 to access Jenkins and execute any necessary additional setup step.

3.Tasks description
------------

1. **Checkout** : checkout the code from the repository

   
2. **Assemble** : compiles and produces the archive files with the application. Do not use
the build task of gradle (because it also executes the tests)!
   
   
3. **Test** : executes the Unit Tests and publish in Jenkins the Test results. See the junit
step for further information on how to archive/publish test results.
Do not forget to add some unit tests to the project (maybe you already have done it).
   
   
4. **Javadoc** : generates the javadoc of the project and publish it in Jenkins. See the
publishHTML step for further information on how to archive/publish html reports.
   

5. **Archive** : archives in Jenkins the archive files (generated during Assemble, i.e., the war
file)
   
   
6. **Publish Image** : generate a docker image with Tomcat and the war file and publish it
in the Docker Hub.

4.Implementation
----------

Define pipeline as code (in a language called Groovy, the same used by Gradle).

By convention the script of the pipeline is stored on files named _**Jenkinsfile**_.

This file is usually located in the root folder of the source code repository.

4.1 . Create initial pipeline
------------------------

1. Pipeline configuration:

Go to **Configure_ and define the repository and set the path for the _Jenkinsfile_.

-Note: the path if the _Jenkinsfile_ is relative to the root of the repository, in my case is ```https://bitbucket.org/MiguelLoureiro1181945/devops-20-21-1181945/src/master/```, so in my case is on folder ```ca2/part2/tut-basic-gradle```.

![img](https://i.imgur.com/ziBJsuv.png)

![img](https://i.imgur.com/VlgBEqs.png)


-Note: since my repository is _private_ a credential must be supplied to have access.


Create a very simple pipeline using the "tut-basic-demo" application to have the following stages in my _Jenkinsfile_:

. **Checkout**: to checkout the code form the repository

    - Edit the _Jenkinsfile_ to have the following:

    ```
     stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'miguelloureiro-bitbucket-credentials', url: 'https://bitbucket.org/MiguelLoureiro1181945/devops-20-21-1181945/src/master/ca2/part2/tut-basic-gradle/'

            }
        }
    ```
  
4.2 Execute initial pipeline
--------------

1. Build #1

   ![img](https://i.imgur.com/DbiHWuH.png)

- Note: build successful.

4.3 Add more tasks to initial pipeline
----------------------

- Edit the _Jenkinsfile_ and add the following:


. **Assemble**: compiles and produces the archive files with the application.
   Note: do not use the build task of gradle (because it also executes the tests)!

- Edit the _Jenkinsfile_ to have the following:

    ```
     stage('Assemble') {
                    steps {
                        echo 'Assembling...'
                        script {
                           if (isUnix()) {
                           dir ('ca2/part2/tut-basic-gradle') {
                           sh './gradlew clean assemble'
                           }
                           }

                           else {
                           dir ('ca2/part2/tut-basic-gradle'){
                           bat 'gradlew.bat clean assemble'
                           }
                           }

                        }
                    }
        }
    ```

-   _Note_ : the condition  ( if ... else ) is to allow the execution on _Unix (Mac laptops)_ or _Windows_ laptops.

. **Test**:. Executes the Unit Tests and publish in Jenkins the Test results.
   Note: see the _junit_ step for further information on how to archive/publish test results.

   Note: do not forget to add some unit tests to the project (maybe you already have done it).

- Edit the _Jenkinsfile_ to have the following:

    ```
    stage ('Test'){
            steps {
                echo 'Testing...'
                script {
                    if (isUnix()) {
                    dir ('ca2/part2/tut-basic-gradle') {
                    sh './gradlew test'
                    junit '**/TEST-*.xml'
                    }

                    }

                    else {
                    dir ('ca2/part2/tut-basic-gradle') {
                    bat 'gradlew.bat test'
                    junit '**/TEST-*.xml'
                    }

                    }

                    }
            }

        }

    ```

4. **Javadoc**  : generates the javadoc of the project and publish it in Jenkins.

- Note : See the **publishHTML** step for further information on how to archive/publish html reports.
  
- Important: please install **Javadoc** and **HTML Publisher** plugins before continue. If already installed please ignore this step.

Using the **Pipeline Syntax "Snippet Generator"** to help me generate a code snippet for _javadoc_ generation, like the following:

![img](https://i.imgur.com/VxB7Teb.png)


- Edit the _Jenkinsfile_ to have the following:

    ```
    javadoc javadocDir: 'ca2/part2/tut-basic-gradle', keepAll: true
    ```

- Note: `gradle javadoc` command will create a folder called '\docs\javadoc' inside project build folder, i.e., 'ca2/part2/tut-basic-demo/build'. 
  
Also, for publishing the javadoc, please use a code snippet like the following: 


![img](https://i.imgur.com/Djfv0to.png)

- Edit the _Jenkinsfile_ to have the following:
  
```
    publishHTML([allowMissing: false,
        alwaysLinkToLastBuild: false,
        keepAll: false,
        reportDir: 'ca2/part2/tut-basic-gradle/build/docs/javadoc',
        reportFiles: 'index.html',
        reportName: 'HTML Javadoc Report',
        reportTitles: 'Javadoc'])
        
  ```

So this stage will be like this :

```

    stage('Javadoc') {
                    steps {
                    echo 'Generating Javadoc and publishing...'
                    script {
                            if (isUnix()) {
                            dir ('ca2/part2/tut-basic-gradle'){
                            sh './gradlew javadoc'
                            }
                            javadoc javadocDir: 'ca2/part2/tut-basic-gradle', keepAll: true
                            publishHTML([allowMissing: false,
                                         alwaysLinkToLastBuild: false,
                                         keepAll: false,
                                         reportDir: 'ca2/part2/tut-basic-gradle/build/docs/javadoc',
                                         reportFiles: 'index.html',
                                         reportName: 'HTML Javadoc Report',
                                         reportTitles: 'Javadoc'])
                            }
                            else {
                            dir ('ca2/part2/tut-basic-gradle'){
                            bat 'gradlew.bat javadoc'
                            }
                            javadoc javadocDir: 'ca2/part2/tut-basic-gradle', keepAll: true
                            publishHTML([allowMissing: false,
                                         alwaysLinkToLastBuild: false,
                                         keepAll: false,
                                         reportDir: 'ca2/part2/tut-basic-gradle/build/docs/javadoc',
                                         reportFiles: 'index.html',
                                         reportName: 'HTML Javadoc Report',
                                         reportTitles: 'Javadoc'])
                            }
                    }
                    }
        }


   ```


4.4 Execute the tasks added until now
--------------

1. Build #2

   ![img](https://i.imgur.com/FVddnVF.png)

#### Build#2 successful

- Note: try to see the Javadoc HTML report but nothing is shown.

![img](https://i.imgur.com/cZUFvBx.png)

- Possible reasons: no javadoc present in code, or the path is wrong.

2. Build #3

- a. Add some _javadoc_ to Employee.java to see how the HTML Javadoc Report is.

Edited the file `Emplyee.java` and add some javadoc to it, like the example below:

```

    ...
            /**
	        *
	        * @param firstName
	        * @param lastName
	        * @param description
	        * @param email
	        */
	        public Employee(String firstName, String lastName, String description, String email) {

		        checkFirstName(firstName);
		        checkLastName(lastName);
		        checkDescription(description);
		        checkEmail(email);
	        }
	        
	        
	        ...
	        
   ```

- b. Edit the _Jenkinsfile_ and changed the paths as seen below:

    ```
        
        javadoc javadocDir: 'build/docs/javadoc', keepAll: true
         publishHTML([allowMissing: false,
                      alwaysLinkToLastBuild: false,
                      keepAll: false,
                      reportDir: 'build/docs/javadoc',

    ```
  
![img](https://i.imgur.com/TjqeulN.png)

#### Build#3 failed!!!

- Possible reasons: the path is wrong, again!

3. Build #4

Edited again the _Jenkinsfile_ and changed the paths as seen below:

``` 
        javadoc javadocDir: 'ca2/part2/tut-basic-gradle/build/docs/javadoc', keepAll: false
        publishHTML([allowMissing: false,
                     alwaysLinkToLastBuild: false,
                     keepAll: false,
                     reportDir: 'ca2/part2/tut-basic-gradle/build/docs/javadoc',


   ```

![img](https://i.imgur.com/mUzIFiS.png)

#### Build#4 successful

- Important: the HTML Javadoc Report is displayed if the browser is _Internet Explorer_. _Google Chrome_ does not allow inline scripts and therefore CSS, as indicated when inspecting the report page 
  
`Refused to execute inline script because it violates the following Content Security Policy directive: "script-src * 'unsafe-eval'". Either the 'unsafe-inline' keyword, a hash ('sha256-OKx5l3EJln9sotWnL0yWBKUFfnihPc6c5YTiAsXTJBs='), or a nonce ('nonce-...') is required to enable inline execution`

- In _Internet Explorer_ the **HTML Javadoc Report** is rendered normally as seen below:

![img](https://i.imgur.com/U4enKGe.png)


4.5 Add the remaining tasks (Archive and Publish Image)
-------------------------

4.5.1 Archive
---------------

. **Archive**. archives in Jenkins the archive files (generated during Assemble, i.e., the war file)

- Before (important): the **Assemble** stage must produce a _**war**_ file and **NOT** a _**jar**_ file.
  Must add a _plugin_ to _build.gradle_ file to have a resulting _war_ file.

From  previous class assignment (ca3-part2) support for building _war_ file is found [here](https://bitbucket.org/atb/tut-basic-gradle/commits/15eb12ff6cc0bbadb27a57caef5f83ffa5e03126)

Go into folder `ca2/part2/tut-basic-gradle` and edit the _build.gradle_ file to have the following:

```
        plugins {
	        id 'org.springframework.boot' version '2.4.4'
	        id 'io.spring.dependency-management' version '1.0.11.RELEASE'
	        id 'java'
	        id "org.siouan.frontend" version "1.4.1"
	        id 'war'
        }

        group = 'com.example'
        version = '0.0.1-SNAPSHOT'
        sourceCompatibility = '1.8'

        repositories {
	        mavenCentral()
        }

        dependencies {
	        implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	        implementation 'org.springframework.boot:spring-boot-starter-data-rest'
	        implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
	        runtimeOnly 'com.h2database:h2'
	        testImplementation('org.springframework.boot:spring-boot-starter-test') {
            exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
	        }
	        // To support war file for deploying to tomcat
	        providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
        }

    test {
	    useJUnitPlatform()
    }

    frontend {
	    nodeVersion = "12.13.1"
	    assembleScript = "run webpack"
    }

    task copyJar(type: Copy) {
	    from 'build/libs'
	    include '*.jar'
	    into 'src/dist'
    }

    task deleteWebpackFiles(type: Delete) {
	    delete '/src/main/resources/static/built/'
    }

    clean{
	    dependsOn 'deleteWebpackFiles'
    }

   ```

- Now, it is possible to edit the _Jenkinsfile_ to have the following:

    ```
    stage('Archive') {
                    steps {
                        echo 'Archiving...'
                        archiveArtifacts 'ca2/part2/tut-basic-gradle/build/libs/demo-0.0.1-SNAPSHOT.war'
                    }
                }
                
   ```

4.5.2 Execute Archive task
------------------
 
4. Build #5

![img](https://i.imgur.com/Qs6Deju.png)

#### Build#5 successful

- Note: check if the build file is archived in the correct folder.

![img](https://i.imgur.com/eTrGhCc.png)


4.5.3 Publish Image
---------------

- Before (important): please install the necessary _plugins_ to handle _Docker_ operations.

![img](https://i.imgur.com/PbM6pyf.png)

- Note: must have a _Dockerfile_ in the same folder as the _Jenkinsfile_.

a. Create a _Dockerfile_ and edit to have the following:

```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

ADD ca2/part2/tut-basic-gradle/build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080

```

b. Edit the _Jenkinsfile_ and add the following: 

- Note: the tag for the image will be the job build number of Jenkins.

```
    stage ('Docker Image') {
            steps {
            echo 'Publishing image...'
            script {
            docker.build("mloureiro75/ca5-part2:${env.BUILD_ID}")
            }
            }
        }

```

4.5.4 Execute Publish Image task
------------------

5. Build#6

![img](https://i.imgur.com/FxssR8h.png)

![img](https://i.imgur.com/sw6CTod.png)

#### Build#6 failed!!!

- Reasons: forget to put location of _Dockerfile_.

Edit again the _Jenkinsfile_ and add the following:

```
        stage ('Docker Image') {
            steps {
            echo 'Publishing image...'
            script {
            dir ('ca2/part2/tut-basic-gradle') {
            docker.build("mloureiro75/ca5-part2:${env.BUILD_ID}")
            }

            }
            }
        }


```
6. Build#7

![img](https://i.imgur.com/f6emd9A.png)

![img](https://i.imgur.com/cUcPOWb.png)

#### Build#7 failed!!!

Edit again the _Jenkinsfile_ and change in Archive stage, the following:

```
stage('Archive') {
                        steps {
                            echo 'Archiving...'
                            archiveArtifacts 'ca2/part2/tut-basic-gradle/build/libs/*'
                        }
                    }

```

7. Build#8

![img](https://i.imgur.com/61g0idW.png)

![img](https://i.imgur.com/OrfjvQn.png)

#### Build#8 failed!!!

8. Build#9


![img](https://i.imgur.com/0h31pM5.png)

- Image is on Docker

![img](https://i.imgur.com/6dcVFxd.png)


#### Build#9 successful

The full pipeline is executed. 

![img](https://i.imgur.com/MgONEuC.png)
--------------------------------------------------------------------------------------

#Alternative tool :

![img](https://i.imgur.com/8St55LF.png)

## Alternative Analysis

**_Buddy_** is a smart CI/CD tool with awesome User Interface. It uses delivery pipelines to build, test and deploy software. The pipelines are created with over 100 ready-to-use actions (build Steps) that can be arranged in any way – just like you build a house of bricks. Please find more information [here](https://buddy.works/).

- Features :

  . 15-minute configuration in clear & telling UI/UX
  
  . Lightning-fast deployments based on changesets
  
  . Our builds run in isolated containers with cached dependencies
  
  . Supports all popular languages, frameworks & task managers
  
  . Dedicated roster of **Docker/Kubernetes** actions
  
  . Integrates with **AWS**, **Google**, **DigitalOcean**, **Azure**, **Shopify**, **WordPress** & more
  
  . Supports parallelism & YAML configuration

1. Create account [here](https://buddy.works/sign-up)

When creating a new account synchonize with remote repository (GitHub, GitLab, BitBucket).



2. 

![img](https://i.imgur.com/qZsp08K.png)

![img]()

![img]()

![img]()

![img]()



5.Conclusion
----------

Learned how to create a _Jenkins_ pipeline with the creation of several jobs to automate a build.

6.Finish the class assignment
-------

* Add tag `ca5-part2` to the repository