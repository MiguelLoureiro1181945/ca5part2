# Class Assignment 2 Report

# Part 1 :

1.Analysis
------------

The goal of this class assignment is to implement a series of _gradle_ tasks into the example application and describe the implementation of those tasks in a _"tutorial"_ style.

2.Design
-------

Description of the process to manage the implementation of the requirements for this class assignment.

- Task management: 

Tasks are defined as _Bitbucket_ issues.  They were created to set open tasks to resolve. The issues that were open are [here](https://bitbucket.org/MiguelLoureiro1181945/devops-20-21-1181945/issues).

- Issues:

10. new task to execute the server
   
11. unit test and update the gradle script so that it is able to execute the test

12. make a backup of the sources of the application

13. make an archive (i.e., zip file) of the sources of the application

14. finish part1 of assignment with tag **_ca2-part1_**

Issues are set to **resolve** state on completion, by commit message or by the own BitBucket issues function.

3.Implementation
----------
Each task steps is described below.

#### 1. Execute the server
   
- Download the example application from [here](https://bitbucket.org/MiguelLoureiro1181945/devops-20-21-1181945/src/master/ca2/part1/gradle_basic_demo/) into local folder ```.../devops-20-21-1181945/ca2/part1`` and add to the remote repository using:
   
   ``` git add . ``` 
   
   ``` git commit -m "added basic gradle demo" ```
   
   ```git push``` 

    a. 
**Note**: I needed to update the folders and **deleted** the example application by mistake. **Commited this delete.** 

    ``` git add .```

    ``` git commit -m "folder update"```

    ``` git push```

    b. Add again the example application and also the class examples

    ``` git add .```

    ``` git commit -m "add basic gradle demo and class examples```

    ``` git push```

    **Note**: in "normal" conditions (i.e whithout my mistake) this step would be made only with step _2.2_.

- Analysis of the example application

    - Read the instructions available in the _readme.md_ file provided 
    - Experiment the application

[![ca2-p1-2.jpg](https://i.postimg.cc/QtRpFyrC/ca2-p1-2.jpg)](https://postimg.cc/gXHw54jb)

[![ca2-p1-3.jpg](https://i.postimg.cc/XYsZmzqh/ca2-p1-3.jpg)](https://postimg.cc/zyRXRpnS)

- Add a new task to execute the server by editing the file ```build.gradle``` and add the following task:

```
task executeServer(type:JavaExec, dependsOn: classes) {
    group = "DevOps"
    description = "Starts a server"

    classpath = sourceSets.main.runtimeClasspath

    main = "basic_demo.ChatServerApp"

    args '59001'
}
```

- Commit the new task

    ``` git add .```

    ``` git commit -m "add task to execute the server"```
   
    ``` git push ```

[![ca2-p1-5.jpg](https://i.postimg.cc/VLwnTNXH/ca2-p1-5.jpg)](https://postimg.cc/LhyJg2Cz)

- Test the newly added task _executeServer_

[![ca2-p1-6.jpg](https://i.postimg.cc/CxsWpdZP/ca2-p1-6.jpg)](https://postimg.cc/3kWLg8Fm)

**Task 1 completed**

#### 2. Add a simple unit test

- Prepare for unit testing in _gradle_ by editing the gradle script file ``` build.gradle```  so that it is able to execute tests. The unit tests that will be added will require _junit 4.12_ to execute. Add this _dependency_ in
   _gradle_.

```
dependencies {
    ....
    
    // add junit dependency
    testImplementation 'junit:junit:4.12'
    
}
```

- Add the test code provided in the class assignment documentation to the project by creating a new test/java directory under the _src_ folder and add a new file ```AppTest.java```. 
  Edit the file by adding the code provided in the class assignment documentation.
```
package basic_demo;

import org.junit.Test;

import static org.junit.Assert.*;

public class AppTest {
    @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
}
```
- Add the newly created unit test to the repository

[![ca2-p1-7.jpg](https://i.postimg.cc/VLHXchQH/ca2-p1-7.jpg)](https://postimg.cc/F73YyDKj)

- Check if the task was successfully (i.e the folder _backup_ was created, and the _src_ folder content was added into)

[![ca2-p1-16.jpg](https://i.postimg.cc/yNWct2Tv/ca2-p1-16.jpg)](https://postimg.cc/5HdXF7TC)

- Execute all tasks

```./gradlew build```

[![ca2-p1-14.jpg](https://i.postimg.cc/fWc27mSR/ca2-p1-14.jpg)](https://postimg.cc/Lhs32qVc)
    
- Run the unit test
    
``` gradle test```

[![ca2-p1-13.jpg](https://i.postimg.cc/HkgTZQBv/ca2-p1-13.jpg)](https://postimg.cc/JGpwtBwj)

- Create a report for the test

[![ca2-p1-10.jpg](https://i.postimg.cc/Dz6QJw4B/ca2-p1-10.jpg)](https://postimg.cc/qtt6W0L3)

- Report [here](https://scans.gradle.com/s/srfvulqmvqe3u)

**Task 2 completed**

#### 3. Add a new task of type **Copy**

- Add a new task _makeBackup_ to copy the sources of the application to a _backup_ folder by editing the file ```build.gradle``` and add the following task:

```
// Class assignment task#3: make a backup of the sources of the application
task makeBackup(type: Copy) {
    from 'src'
    into 'backup'
}
```
- Execute the task _makeBackup_

[![ca2-p1-15.jpg](https://i.postimg.cc/cLFfDdfv/ca2-p1-15.jpg)](https://postimg.cc/NKryFqJt)

- Check if the task was successfully (i.e., the folder _backup_ was created, and the _src_ folder content was added into)

[![ca2-p1-16.jpg](https://i.postimg.cc/yNWct2Tv/ca2-p1-16.jpg)](https://postimg.cc/5HdXF7TC)

- Commit and push the new task to the remote repository

[![ca2-p1-17.jpg](https://i.postimg.cc/MTyMJt2c/ca2-p1-17.jpg)](https://postimg.cc/wyjThXBH)

**Task 3 completed**

#### 4. Add a new task of type **Zip**

Add a new task _makeZip_ to make an archive (i.e., zip file) of the sources of the application inside folder _.../build/dist/_ by editing the file ```build.gradle``` and add the following task:

```
// Class assignment task#4: execute the server
task makeZip(type: Zip) {
    archiveFileName = "my-sources.zip"
    destinationDirectory = layout.buildDirectory.dir('dist')
    from 'src'
}
```

- Execute the task _makeZip_

[![ca2-p1-18.jpg](https://i.postimg.cc/P53cQ0yd/ca2-p1-18.jpg)](https://postimg.cc/cv3Xs9Pz)

- Check if the task was successfully (i.e., the file _my-sources.zip_ was created inside folder _.../buid/dist/_ )

[![ca2-p1-19.jpg](https://i.postimg.cc/3JCChp06/ca2-p1-19.jpg)](https://postimg.cc/4myhbY5b)

- Commit and push the new task to the remote repository

[![ca2-p1-20.jpg](https://i.postimg.cc/LX86JcxL/ca2-p1-20.jpg)](https://postimg.cc/FfwQqB5H)

**Task 4 completed**

#### 5. Finish the class assignment

- Tag the repository with tag _ca2-part1_

``` git add . ```

``` git commit -m "ca2-part1 finished"```

``` git tag -a ca2-part1```

5.Conclusion
-------

After this class assignment I am comfortable in the usage of some build tools like _Gradle_ and learned how to apply that into an application.