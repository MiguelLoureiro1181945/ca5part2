# Class Assignment 3 Report

# Part 1 :

1.Analysis
------------

Run the applications of the projects from the previous assignments but now inside a VirtualBox VM with Ubuntu.

2.Setup
----------

Here I describe the initial setup before going to implementation.

1. Download VirtualBox and install.

2. Created a Virtual Machine(VM) called _mldevops_ and set the **IP** of the second network adapter as **192.168.56.10** by editing the file _01-netcfg.yaml_ as described in the class documentation

![img](https://i.imgur.com/LXh31Gz.png)

3.Jobs description
------------

A. Build and execute the **_gradle_basic_demo_** project (from the previous class assignments)

B. Build and execute the **_tut-basic-gradle_ project_** (from the previous assignments)

4.Implementation
----------
Each task steps is described below.

* Setup: clone my personal DevOps repository into VM

![img](https://i.imgur.com/SgyXRxM.png)

- For this class assignment I need to go (inside the VM) into **_ca2/part2_** folder of my repository and use _tut-basic_ and _gradleapp_.

![img](https://i.imgur.com/d55cpBG.png)

![img](https://i.imgur.com/xUjaaCH.png)

### Job A:

From the previous class assignment this gradle project has 3 tasks:

![img](https://i.imgur.com/q6N8hvp.png)

1. Execute the server, by executing the command `gradle executeServer`

2. Backup sources of the application, by executing the command `gradle makeBackup`

3. Make zip file of the sources of the application, by executing the command `gradle makeZip`

##### Task 1:

1. Build the _gradle_basic_demo_ project in the Virtual Machine (VM) running the command ``` ./gradlew build``` inside the project folder

   ![img](https://i.imgur.com/vMkMjtv.png)

* Problem:
    - Build not successfully at first attempt. Why ??? Checking on file read/write permissions and see that _gradlew_ has only _read_ and _write_ permissions, not _execution_ permissons.

  ![img](https://i.imgur.com/0uRjI8T.png)

* Solution:
    - Grant _execution_ permission to _gradlew_ command  with `chmod +x gradlew` and build the project

  ![img](https://i.imgur.com/uhxM2aa.png)

2. Run the application in the VM

   `./gradlew run`

   ![img](https://i.imgur.com/gRj8ppA.png)

3. Start the _server_ in the VM with command `gradle executeServer´

   ![img](https://i.imgur.com/HkHruwK.png)

4. Run client _Maria_ on the _host_ machine

   a. Open one terminal on the _host_ machine and run a client.

   ```gradle runClient```

   ![img](https://i.imgur.com/06g4Hxq.png)

   b. Open another terminal on the _host_ machine and run another client _Tony_.

   ````gradle runClient```

   ![img](https://i.imgur.com/AMQ4LyL.png)

5. Check that on the VM the 2 clients are initiated

   ![img](https://i.imgur.com/MsGcLCS.png)

   ![img](https://i.imgur.com/uCY28TT.png)

   ![img](https://i.imgur.com/tinWUY6.png)


* The application is executed in the VM and is accessed in the _host_ machine via the VM IP address showing the values

##### Task 2:

1. Make the backup of the application sources and check that the backup was made

![img](https://i.imgur.com/zcYUIW0.png)

##### Task 3:

1. Make the zip file of the application sources and check that the zip file was made

![img](https://i.imgur.com/eJjLU2b.png)
______________________

### Job B:

From the previous class assignment, this application has

1. Build and execute the _tut-basic-gradle_ demo application (from the previous class assignment)  (_gradle-basic-demo_) in the Virtual Machine (VM) using the command _./gradlew build_

![img](https://i.imgur.com/OKXdSQl.png)

* Problem:
    - The application is not running. The _gradlew_ file has no execution permission.

* Solution:
    - Grant execution permission to the file with `chmod +x gradlew`

![img](https://i.imgur.com/8XKuStz.png)

2 . Try to _build_ the application again

![img](https://i.imgur.com/3FTZ29L.png)

* Build successful on the VM

3. Start the Spring boot with the command ´./gradlew bootRun´

![img](https://i.imgur.com/F60zjML.png)

3. Check if I can access the application results on the _host_ machine by going to ´192.168.56.10:8080' in the browser

![img](https://i.imgur.com/L08Q4kO.png)


5.Conclusion
----------

* After this class assignment part1 I´m now able to start a virtual machine using _VirtualBox_.

6.Finish the class assignment
-------

* Add tag `ca3-part1` to the repository

---------------------------------------------------------------------------------

# Class Assignment 3 Report

# Part 2 :

1.Analysis
------------

Run the applications of the projects from the previous assignments using VirtualBox VM with Ubuntu and the creation of multiple virtual machines being managed by Vagrant for web application and database (persistance) and also try to implement an alternative technological solution for the virtualization tool.

2.Implementation
----------
Using _VirtualBox_ virtualization tool

![img](https://i.imgur.com/Ce9tcvG.png)

### STEPS

1. Copy the _Vagrantfile_ from [here](https://bitbucket.org/atb/tut-basic-gradle/src/master/) to folder _.../ca3/part2_.


2. Check that the file is inside folder

![img](https://i.imgur.com/WppVgxd.png)

3. Update the _Vagrantfile_ configuration so that it uses your own gradle version of the
   spring application

* Using the workflow (from the commits) described [here](https://bitbucket.org/atb/tut-basic-gradle/commits/) make the changes necessary

![img](https://i.imgur.com/rZ8biUB.png)
![img](https://i.imgur.com/aYZL5k6.png)

4. Start the virtual machines (web and db) with the command `vagrant up`. This will set up the 2 _virtual machines_.

![img](https://i.imgur.com/lBJp2mE.png)

![img](https://i.imgur.com/8ToEXF9.png)

5. Check the virtual machines that are on

![img](https://i.imgur.com/f7Gn3D5.png)

6. Go into the _web_ virtual machine by starting an SSH session to the VM with the command `vagrant ssh web`

![img](https://i.imgur.com/Y7CMgRc.png)

![img](https://i.imgur.com/7QE5c3U.png)

7. Check that it made the _war_ file by checking if file exists in folder _.../ca3/part2/tut-basic-gradle/build/libs_

![img](https://i.imgur.com/maD4itE.png)

8. Build the application inside the _web virtual machine_ with command `./gradlew build`

![img](https://i.imgur.com/Cgll4lB.png)

![img](https://i.imgur.com/aJ1siJg.png)

* BUILD SUCCESSFUL

9. Check that on the _host machine_ I can see the data presented by going into ´http://localhost:8080/demo-0.0.1-SNAPSHOT/´

![img](https://i.imgur.com/aya0GFY.png)

10. Check that on the _host machine_ I can remotely access the database going into ´http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console´

![img](https://i.imgur.com/mcRsgYN.png)

11.Change the **JDBC URL** as according to what is on _application.properties_ file

![img](https://i.imgur.com/RhxJbP3.png)

![img](https://i.imgur.com/OCiZkxs.png)

12. Execute a simple query to the Database

![img](https://i.imgur.com/kMRxCBU.png)

* Conclusion : everything ok

3.Alternative technological solution - **Hyper-V**
----------

![img](https://i.imgur.com/Ucx6dKa.png)

As a _Windows_ user I will use the Hyper-V.

Hyper-V is a Microsoft technology that allows users to create virtual computer environments, and run and manage multiple operating systems on a single physical server, i.e., a software that allows you to run one or multiple virtual machines with their own operating systems (guest operating systems) on a physical computer, which is called a host machine.

1. Create a new _alternative_ folder and move into it. Copy the _Vagrantfile_ used before

![img](https://i.imgur.com/LqgKtOW.png)

* Note: the _Vagrantfile_ used in the _VirtualBox_ implementation

2. Edit the _Vagrantfile_ and change the provided box to `bento/ubuntu-18.04`

![img](https://i.imgur.com/f5WnNLZ.png)

3. Hyper-V provision with `vagrant up --provider hyperv`

This will boot it up for you (notice that I am specifying the provider, which is Hyper-V in our case, by default Vagrant uses VirtualBox)

![img](https://i.imgur.com/5EmqkxB.png)


* Problem:
    - must open console with administrative privileges

* Solution:
    - open console with administrator rights

      ![img](https://i.imgur.com/ESnE858.png)

      ![img](https://i.imgur.com/DD4PpmU.png)

4. Check that the 2 _VM machines_ were created and are running

![img](https://i.imgur.com/OULpxhE.png)

5. Access to the _web virtual machine_ using _ssh_

![img](https://i.imgur.com/G75Usg4.png)

6. Check on the Hyper-V Manager on _Windows_ the IP adresses of _web_ and _db_
    * This is done by typing "Hyper-V" on the _Windows search bar and it will open the manager

![img](https://i.imgur.com/yktWfmn.png)

* Notice that the **IP address** for the _db_ is **172.29.219.111**

7. Changed the application.properties file to retrieve the database info from the correct VM IP

![img](https://i.imgur.com/hTi72rv.png)

8. Check the _web_ **IP address**

![img](https://i.imgur.com/IgCQ2U5.png)

* Notice that the **IP address** for the _web_ is **172.29.223.163**

![img](https://i.imgur.com/rYbcpq2.png)

9. Check the _frontend_ with `http://172.29.223.163:8080/demo-0.0.1-SNAPSHOT/`

![img](https://i.imgur.com/NFuqCw1.png)

10. Check the database console with `http://172.29.223.163:8080/demo-0.0.1-SNAPSHOT/h2-console/`

![img](https://i.imgur.com/9oOhH6Q.png)

11.Change the **JDBC URL** as according to what is on _application.properties_ file

![img](https://i.imgur.com/xBV6lWE.png)

12. Execute a simple query to the Database

![img](https://i.imgur.com/wK7HPdz.png)

* Conclusion : everything ok

4.Conclusion
-------

### Considerations about _VirtualBox_ and _Hyper-V_

* Hyper-V is a **type** 1 hypervisor that is also called a **_bare metal_** hypervisor, and runs directly on a computer’s hardware. When a physical computer (a host) starts, a Hyper-V hypervisor takes control from BIOS or UEFI. Then, Hyper-V starts the management operating system, which can be Hyper-V Server, Windows, or Windows Server. Virtual machines can be started manually by user or automatically, depending on its settings.


* Hyper-V is always on if the host is powered on, while the VirtualBox can be started and closed by a user on demand.


* VirtualBox is a **type 2** hypervisor that is sometimes called a **_hosted_** hypervisor. A type 2 hypervisor is an application that runs on the operating system (OS) and is already installed on a host. When a physical computer starts, the operating system installed on the host loads and takes control. A user starts the hypervisor application (VirtualBox in this case) and then starts the needed virtual machines. VM hosted processes are created.

![img](https://i.imgur.com/zZ8QyKW.png)

### Differences between _VirtualBox_ and Hyper-V

| Item | VirtualBox | Hyper-V |
| -----------|---------|--------|
| Easy to use means | Easy |Complicated |
| Acting | Medium | Good |
| Snapshots | Yes | No |
| Share files | Yes | Yes, but complicated |
| Encryption | Yes (via guest additions) | Yes |
| Compatible systems | Windows, Linux, macOS | Windows and Linux (this with limitations)|
| Price | Free | Free |
| Others | Open source | Windows 10 (Home*) |

* via a _third party_ enabler

### Limitations of _Hyper-V_


When using Hyper-V, you cannot use VirtualBox (5.x) at the same time to run VMs; you need to make a choice and switching afterwards requires a Windows restart. The reason for this is that when Hyper-V is running, Hyper-V claims the hardware resources required to provide virtualization and provides an abstraction on top. The only way to access those resources is through an Hyper-V interface. This is also the reason why Hyper-V running on top of Hyper-V works and other virtualization technologies on top of Hyper-V are often an issue (unless they implement the Hyper-V interface). When VirtualBox is running, VirtualBox asks the host for access to the required resources and not Hyper-V. When Hyper-V is not running underneath the host, the host can provide this. Otherwise the host is (more or less) just a VM running on Hyper-V and cannot help VirtualBox with its requirements (something like ‘the wrong person to ask’).

5.Finish the class assignment
----------

* Add tag `ca3-part2` to the repository