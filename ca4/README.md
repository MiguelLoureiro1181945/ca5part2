# Class Assignment 4 Report

1.Analysis
------------

Use _Docker_ to setup a containerized environment to execute your version of the _gradle_ version of _spring basic tutorial application_.

2.Setup
----------

1. Install _Docker_ from [here](https://www.docker.com/)

* As a Windows user I install _Docker Desktop_ from [here](https://www.docker.com/get-started)


3.Implementation
----------
Each task steps is described below.

1. Open a terminal on this class assignment folder

![terminal in ca4 folder](https://i.imgur.com/exvrnCO.png)

2. Create three folders called _web_, _db_ and _data_

![folders creation](https://i.imgur.com/xrGjxHR.png)

![folders web, db and data](https://i.imgur.com/5NWUUut.png)

3. Create a new empty _docker-compose.yml_ file  with command `touch docker-compose.yml`

- Here I use a Docker tool called _Docker Compose_

    * Docker Compose : Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML (*.yml) file to configure your applicationâ€™s services. Then, with a single command, you create and start all the services from your configuration.

![empty docker-compose.yml](https://i.imgur.com/GqdGjfD.png)

4. Edit the file _docker-compose.yml_ to set up 2 containers (_web_ and _db_)

* From the example given in the class example [here](https://bitbucket.org/atb/docker-compose-spring-tut-demo/src) put the following code inside the _docker-compose.yml_ file

```
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.33.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data
    networks:
      default:
        ipv4_address: 192.168.33.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24

```
Notice:

* **web** : will run _Tomcat_ and the spring tutorial application (_tut-basic-gradle_)

* **db** : will run _H2 Database_


5. Move into _web_ folder and create a new empty _DOCKERFILE_

- Here a use a _dockerfile_

      * Dockerfile :  is a text document that contains all the commands a user could call on the command line to assemble an image.

 ```
cd web
touch DOCKERFILE
cd ..
```

![wwb dockerfile](https://i.imgur.com/D7wIzNe.png)

6. Edit the _DOCKERFILE_ with the following code:

```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://MiguelLoureiro1181945@bitbucket.org/MiguelLoureiro1181945/devops-20-21-1181945.git

RUN cd devops-20-21-1181945

RUN cd ca3
      
RUN cd part2

RUN cd tut-basic-gradle
      
RUN chmod u+x gradlew

RUN ./gradlew clean build

WORKDIR /tmp/build/tut-basic-gradle-docker

RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080

```

![edit DOCKERFILE web](https://i.imgur.com/sUc4ZVu.png)


7. Move into _db_ folder and create a new empty _DOCKERFILE_

 ```
cd db
touch DOCKERFILE
cd ..
```

![db dockerfile](https://i.imgur.com/zEOSbMR.png)

8. Edit the _DOCKERFILE_ with the following code:

```
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

```

![edit DOCKERFILE db](https://i.imgur.com/Z4GSS7r.png)

9. Created a user _mloureiro75_ in _dockerhub_

![docker-hub user](https://i.imgur.com/v1dhzGV.png)

* found [here](https://hub.docker.com/u/mloureiro75/content)

10. Move into _web_ folder


11. Run `docker build -t mloureiro75/devops-1181945-ca4-web . ` to build your Docker image.

![build web image1](https://i.imgur.com/FuW8uHV.png)
![build web image2](https://i.imgur.com/K5QlT9x.png)

* Build done

12. Run `docker run mloureiro75/devops-1181945-ca4-web` to test your Docker image locally.


![run image web](https://i.imgur.com/ndKYQC9.png)

![run web image3](https://i.imgur.com/0KPuwBh.png)

* Image is running OK

13. Run `docker push mloureiro75/devops-1181945-ca4-web` to push your Docker image to Docker Hub. You should see output similar to:

![push web image1](https://i.imgur.com/71PYOdB.png)

![push web image2](https://i.imgur.com/7ZNu2Uz.png)


![push web image1](https://i.imgur.com/BYlzWmE.png)

![push web image2](https://i.imgur.com/YdGbx9k.png)

* web image is pushed
  ![webimagepushed](https://i.imgur.com/F5xTHfn.png)

![webimagerepo](https://i.imgur.com/jBssaDL.png)

14. Move into _db_ folder

15. Run `docker build -t mloureiro75/devops-1181945-ca4-db .` to build your Docker image.

![build db image1](https://i.imgur.com/vSf8gGu.png)

![build db image2](https://i.imgur.com/M0SJmSE.png)

![build db image1](https://i.imgur.com/1eJJoPQ.png)

![build db image2](https://i.imgur.com/1PGorfa.png)

16. Run `docker run mloureiro75/devops-1181945-ca4-db` to test your Docker image locally.

![run db image1](https://i.imgur.com/obsKSem.png)

![run db image2](https://i.imgur.com/TolA6nu.png)

![run db image1](https://i.imgur.com/UExC86h.png)

![run db image2](https://i.imgur.com/Eyr3AqB.png)

17. Run `docker push mloureiro75/devops-1181945-ca-db`  to push your Docker image to Docker Hub. You should see output similar to:

8

![push db image1](https://i.imgur.com/nUvXL5Q.png)

7

![push db image2](https://i.imgur.com/gJf829q.png)
6

![push db image1](https://i.imgur.com/LnbFTkG.png)

5
![push db image2](https://i.imgur.com/cWSCzYE.png)


* db image is pushed

![Imgur](https://i.imgur.com/5257D7K.png)

![dbimagerepo](https://i.imgur.com/8I7BDsf.png)


18. Move to the root folder (i.e., the folder that contains the yml file) and check the "composition" with the command `nano docker-compose.yml`


![img](https://i.imgur.com/iEuxWad.png)

* Note:

    - The _docker_ composition is done with 2 containers

19. Build the "composition" with command `docker-compose build`

![img](https://i.imgur.com/t6Ysvwe.png)

![img](https://i.imgur.com/WagEFVM.png)

20. Run the _docker_  composition with `docker-compose up` in the folder that contains the _yml_ file
![start docker containers](https://i.imgur.com/5kkZ9gg.png)

![img](https://i.imgur.com/EcFOqlm.png)

![img](https://i.imgur.com/CVLRt7E.png)


21. Check if I can access the _web container_ in the localhost by opening a new browser window url (http://localhost:8080/demo-0.0.1-SNAPSHOT/)

![img](https://i.imgur.com/NrmtRox.png)

22. Check if I can access the _db container_ in the localhost by opening a new browser window url (http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console)

Note: connection string use `jdbc:h2:tcp://192.168.33.11:9092/./jpadb`

![img](https://i.imgur.com/5DPfxxA.png)


23 . Perform a simple query to the database in the localhost

![img](https://i.imgur.com/Ga8jqeu.png)

* Data is obtained in the localhost

24. Check the container ID´s with the command `docker ps`

![img](https://i.imgur.com/x2LMl1Z.png)

* Note:

  - _web_ container ID: c0c2c604aa85
  
  - _db_ container ID: dc03cadfcd84

25. Check the _db_ container to see the database data (db file) with `docker exec -it <db container ID> bash` to open a terminal in the container

![img](https://i.imgur.com/MW55Dkk.png)

* Error: is not teletype

* Solution : as the console indicates must add `winpty` prefix to last command

26. Check the _db_ container to see the database data (db file) with command `winpty docker exec -it <db container ID> bash`

![img](https://i.imgur.com/V24rp4c.png)

* Note: the **database** data file is _jpadb.mv.db_ 

27. Exit the container

![img](https://i.imgur.com/iN11r3K.png)

* back to BASH terminal

28. Copy the database data file from the docker container to the host with command `docker cp dc03cadfcd84:/usr/src/app/jpadb.mv.db /c/reposml/devops/devops-20-21-1181945/ca4/data/`

  *Note: docker copy command is
   `docker cp <containerId>:/file/path/in/container/file /host/local/path/file`

  a. Check that the database is **not** present in the _data_ folder

![databasefilnotpresent](https://i.imgur.com/Qc4HWmh.png)

  b. Copy the database data file and check that it is inside the folder

![databasedatacopied](https://i.imgur.com/LliPXSS.png)

4.Alternative
--------------------------

# **Kubernetes** 


   
###Overview


  **Kubernetes**, in short, is an open source system for managing clusters of containers. To do this, it provides tools for deploying applications, scaling those application as needed, managing changes to existing containerized applications, and helps you optimize the use of the underlying hardware beneath your containers. Kubernetes is designed to be extensible and fault-tolerant by allowing application components to restart and move across systems as needed.
  **Kubernetes** is itself not a Platform as a Service (PaaS) tool, but it serves as more of a basic framework, allowing users to choose the types of application frameworks, languages, monitoring and logging tools, and other tools of their choice. In this way, Kubernetes can be used as the basis for a complete PaaS to run on top of.
  The **Kubernetes** project is written in the _Go_ programming language, and you can browse its source code on GitHub.

###How does Kubernetes work? 
   
  The main unit of organization in **Kubernetes** is called a _pod_. A _pod_ is a group of containers that are administered as a group on the same machine or virtual machine, a node, and are designed to be able to talk to one another easily.
  These pods can be organized into a service, which is a group of pods that work together, and can be organized with a system of labels, which allow metadata about objects like pods to be stored in Kubernetes.
  All of these parts can be orchestrated in a consistent and predictable way through an API, through predefined instructions, and through a command-line client.

  - Cluster

  The central component of Kubernetes is the cluster. A cluster is made up of many virtual or physical machines that each serve a specialized function either as a master or as a node. Each node hosts groups of one or more containers (which contain your applications), and the master communicates with nodes about when to create or destroy containers. At the same time, it tells nodes how to re-route traffic based on new container alignments. The following diagram depicts a general outline of a Kubernetes cluster:


![k8slayout](https://i.imgur.com/oZzk00I.png)


  - Nodes

  All nodes in a Kubernetes cluster must be configured with a container runtime, which is typically Docker. The container runtime starts and manages the containers as they’re deployed to nodes in the cluster by Kubernetes. Your applications (web servers, databases, API servers, etc.) run inside the containers.

  - Pod

  The basic scheduling unit is a pod, which consists of one or more containers guaranteed to be co-located on the host machine and can share resources. Each pod is assigned a unique IP address within the cluster, allowing the application to use ports without conflict. You describe the desired state of the containers in a pod through a YAML or JSON object called a Pod Spec. These objects are passed to the kubelet through the API server. A pod can define one or more volumes, such as a local disk or network disk, and expose them to the containers in the pod, which allows different containers to share storage space. 
  For example, volumes can be used when one container downloads content and another container uploads that content somewhere else.

![img](https://i.imgur.com/hDDjfXM.png)

###Implementation

1. Download _kubectl.exe_ from [here](https://technology.amis.nl/platform/installing-minikube-and-kubernetes-on-windows-10/)


2. Place _kubectl.exe_ in some folder and add the folder path in classpath (mine is “C:\kube”)


3. On _Command Line_ (CMD) enter `kubectl` to install


  *Note: if there are no errors, then **kubectl** is correctly installed

4: Download **minikube** from [here](https://github.com/kubernetes/minikube/releases). Please download _minikube-windows-amd64.exe_.

*Notice: Don’t install using **curl/ chocolatey** as the latest version might have some issues

5. Rename the downloaded _minikube-windows-amd64.exe_ file as minikube.exe and place it to the same folder where you have kubectl.exe, in that way you avoid adding another location in Path.

![kubectlandminikubeplace](https://i.imgur.com/SDSWTPA.png)

6. Start **minikube** with command `minikube start`

![img](https://i.imgur.com/DIFVj9P.png)

7. Check the status of the _cluster_ with command `minikube status`

![img](https://i.imgur.com/ymhrN8H.png)

8. Check the **ip** of the _cluster_ with command `minikube ip`

![img](https://i.imgur.com/WkkJaqw.png)

9. Check the nodes 

![img](https://i.imgur.com/WwLKz5a.png)

* Cluster is running ok

10. Create a namespace where to deploy my **Kubernetes** Pods and Services with command `kubectl create namespace ca4alt`

![img](https://i.imgur.com/AQobSmq.png)

* _ca4alt_ space is created

*Note: I moved into _alternative_ folder, but should have done this before. No problems arise with moving ate this moment


11. Go to your personal **Docker Hub** and check your images inside

![img](https://i.imgur.com/P4Wi5x7.png)

*Notice:

- _web_ : mloureiro75/devops-1181945-ca4-web [here](https://hub.docker.com/repository/docker/mloureiro75/devops-1181945-ca4-web)

- _db_  : mloureiro75/devops-1181945-ca4-db [here](https://hub.docker.com/repository/docker/mloureiro75/devops-1181945-ca4-db)


11. Create the manifest file that is used for creating pods and services

- Web Service: enter the command `kubectl create deployment ca4altweb --image=mloureiro75/devops-1181945-ca4-web:latest --dry-run -o=yaml > deployment.yaml`

![img](https://i.imgur.com/Fq1Vqz0.png)

 * Check the content of _deployment.yaml_ file with command `nano ca4alt-deployment.yaml`

![img](https://i.imgur.com/ev3SNV0.png)

12. Create a **_Service_** to expose the pod with command `kubectl create service clusterip ca4alt --tcp=8080:8080 --dry-run -o=yaml > webservice.yaml`

![img](https://i.imgur.com/Gh0TBe6.png)

*Notice : 2 _yaml_ files 

![img](https://i.imgur.com/EIJU35G.png)

13. Create Pods and Services 

  a. Pods : with command `kubectl create -f deployment.yaml -n ca4alt`

  b. Services: with command `kubectl create -f webservice.yaml -n ca4alt`

![img](https://i.imgur.com/oipUsij.png)

14. Check that everything is ok and running with command `kubectl get all -n ca4alt`

![img](https://i.imgur.com/LAKS0mz.png)

*Pod ID : _pod/ca4altweb-595b88fc59-bblbg_

15. Check the logs (see if the SpringBoot is running) with command `kubectl logs pod/ca4altweb-595b88fc59-bblbg -n ca4alt`

![img](https://i.imgur.com/6MhQ43O.png)

*Note: application is running

16. Set default namespace as "ca4alt" before moving forward with command `kubectl config set-context --current --namespace=ca4alt`

![img](https://i.imgur.com/WIwdIP1.png)

17. Enable port forwarding for accessing Spring boot application with command `kubectl port-forward service/ca4alt 8080:8080 -n ca4alt`

![img](https://i.imgur.com/olTUrrC.png)

18. Access the Spring boot application on the local machine

![img](https://i.imgur.com/fTs6Yyf.png)

*Note: only have access to _Apache/Tomcat_ meaning I was not able to set the H2 Database and because of that the data is not visible


5.Conclusion
----------

After this class assignment I was able to deploy a _SpringBoot_ application in two containers using **Docker**.
For the alternative I choosed to use **Kubernetes** but I was not able to complete the alternative implementation.

6.Finish the class assignment
-------

* Add tag `ca4` to the repository