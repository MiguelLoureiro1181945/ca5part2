package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    String INVALIDNAME = "Not a valid name";
    String INVALIDDESCRIPTION = "Not a valid description";
    String INVALIDEMAIL = "Not a valid email";

    @DisplayName("Fail #1: Assert that a null, blank or empty first name will return a error message")
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    void ShouldReturnNullBlankOrEmptyFirstNameReplacedByInvalidNameMessage(String input) {
        String expected = INVALIDNAME;
        Employee employee = new Employee(input, "Baggins", "ring bearer", "frodo@mail.com");
        String result = employee.getFirstName();
        assertEquals(expected, result);
    }

    @DisplayName("Fail #2: Assert that a null, blank or empty last name will return a error message")
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    void ShouldReturnNullBlankOrEmptyLastNameReplacedByInvalidNameMessage(String input) {
        String expected = INVALIDNAME;
        Employee employee = new Employee("Sam", input, "gardener", "sam@mail.com");
        String result = employee.getLastName();
        assertEquals(expected, result);
    }

    @DisplayName("Fail #3: Assert that a null, blank or empty description will return a error message")
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    void ShouldReturnNullBlankOrEmptyDescriptionReplacedByInvalidNameMessage(String input) {
        String expected = INVALIDDESCRIPTION;
        Employee employee = new Employee("Legolas", "GreenLeaf", input, "legolas@mail.com");
        String result = employee.getDescription();
        assertEquals(expected, result);
    }

    @DisplayName("Fail #4: Assert that a null, blank or empty email will return a error message")
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    void ShouldReturnNullBlankOrEmptyEmailReplacedByInvalidNameMessage(String input) {
        String expected = INVALIDEMAIL;
        Employee employee = new Employee("Meriadoc", "Brandybuck", "friend", input);
        String result = employee.getEmail();
        assertEquals(expected, result);
    }
}